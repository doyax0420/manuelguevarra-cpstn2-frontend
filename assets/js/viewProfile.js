let token = localStorage.getItem("token")
let profileContainer = document.querySelector("#profileContainer")

let params = new URLSearchParams(window.location.search)

//get returns the value of the key passed as an argument, then we store it in a variable
let userId = params.get('userId')
console.log("user id: " + userId)

fetch(`https://doyax0420.herokuapp.com/api/users/details`,{
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	//console.log(data)
		
		profileContainer.innerHTML = 
			`<a class="btn btn-info btn-block" href="./editProfile.html?userId=${userId}"><i class="fas fa-user-edit"></i> Edit Profile</a>
				<div class="col-md-6">
					<div class="card1">
					  <div class="card-block">
					    <h4 class="card-title mt-2">${data.fullName}</h4>
					    <p class="card-text">
					    	Email: ${data.email}<br>
					    	Mobile Number: ${data.mobileNo}
					    </p>       
					  </div>        
					</div>
				</div>
			`
})