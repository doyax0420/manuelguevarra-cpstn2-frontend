let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()
	
	let fullName = document.querySelector("#fullName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value
	//validate to enable submit button when all fields are populated and both passwords match
	if((password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)){
		//check if email exists
		//alert("all good")
		fetch('https://doyax0420.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === false){

				fetch('https://doyax0420.herokuapp.com/api/users',{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						fullName: fullName,
						email: email,
						mobileNo: mobileNo,
						password: password1,												
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data === true){
						alert("Successfully registered")
						window.location.replace("./login1.html")
					}else{
						alert("registration failed")
					}
				})
			}else{
				alert("Email already exist")
				window.location.replace("./login1.html")
			}
		})
	}else{
		alert("Something went wrong. Please re-enter your details.")
	}
})