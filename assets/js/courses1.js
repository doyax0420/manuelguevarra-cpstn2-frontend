let adminUser = localStorage.getItem("isAdmin")

let card1 = document.querySelector("#card1")

let card3 = ""


	fetch('https://doyax0420.herokuapp.com/api/courses/isAdmin')
	.then(res => res.json())
	.then(data => {

		if(data.length < 1){
			card3 = "No courses available"
		}else{
			data.slice(0,3).map( course1 => {
				card3 += 
				`
				<div class="col-lg-4 my-4">
					<div class="bg-success">
					  <img class="card-img-top" src="./assets/img/courses.png" alt="Card image cap">
						  <div class="card-body align-center">
						    <h4 class="card-title">${course1.name}</h4>
						    <p class="card-text">
						      ${course1.description}
						    </p>
						    <p class="priceText text-center">PRICE: $${course1.price}</p>
						  </div>
					</div>
				</div>
				`
				}).join("")
		}
		card1.innerHTML = card3
	})	


