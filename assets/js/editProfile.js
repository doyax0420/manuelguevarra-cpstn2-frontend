let token = localStorage.getItem("token")

let params = new URLSearchParams(window.location.search)

let userId = params.get('userId')

//console.log(courseId)

let fullName = document.querySelector("#fullName")
let mobileNo = document.querySelector("#mobileNo")

fetch('https://doyax0420.herokuapp.com/api/users/details',{
  headers: {
    'Authorization': `Bearer ${token}`
  }
})
  .then((res) => res.json())
  .then((data) => {
  	console.log(data)
  	fullName.value = data.fullName
    mobileNo.value = data.mobileNo
    
  	document.querySelector("#editProfile").addEventListener("submit", (e) => {
  		e.preventDefault()
      //alert("hello")

  		let fName = fullName.value
      let mob = mobileNo.value
      
  		fetch(`https://doyax0420.herokuapp.com/api/users`, {
  			method: "PUT",
  			headers: {
  				'Content-Type': 'application/json',
  				'Authorization': `Bearer ${token}`
  			},
  			body: JSON.stringify({
          userId: userId,
  				fullName: fName,
  				mobileNo: mob                   
         })
  		})
  		.then(res => res.json())
  		.then(data => {
        console.log(data)
  			if(data === true){
          alert("Profile Updated")
  			 //console.log(data)
        	window.location.replace('./profile.html')
  			}else{
  				alert("Something went wrong")
  			}
  		})
  	})
  }); 
