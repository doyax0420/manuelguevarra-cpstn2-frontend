let params = new URLSearchParams(window.location.search)

let courseId = params.get('courseId')

let token = localStorage.getItem('token')

fetch(`https://doyax0420.herokuapp.com/api/courses/${courseId}`, {
	method: "DELETE",
	headers: {
		'Authorization': `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {
	if(data === true){
		window.location.replace('./courses.html')
	}else{
		alert("Something went wrong")
	}
})
