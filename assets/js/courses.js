let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector("#adminButton")
let card1 = document.querySelector("#card1")
let cardFooter;
let cardBody;
let card3;


if(adminUser === "false" || adminUser === null){
// console.log(adminUser)

	fetch('https://doyax0420.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		//console.log(data)
     
		let courseData;
		//if the number of courses fetched is less than 1 display no courses
		if(data.length < 1){
			courseData = "No courses available"
		}else{
			//iterate the courses collection and display each course
			
			courseData = data.map(course => {
			//console.log(data)
				//check if the user is an admin, display the edit and delete button of they are
					cardFooter = 
					`
						<a id="button-css" href="./course.html?courseId=${course._id}" >Enroll Now</a>
					`			
				return( 
					`
						<div class="card">
							<div class="circle">
								<h6>${course.name}</h6>
							</div>
							<div class="content">
								<p class="text-white">${course.description}</p>
								<p class="text-white"><em>PRICE: </em>${course.price}</p>
								${cardFooter}
							</div>
						</div>
					`
				)
			}).join("")
		}
		let container = document.querySelector("#coursesContainer")
		container.innerHTML = courseData

		

		let x = 0;

		data.map(counter => {
			
			let symbols,color;
			
			symbols ="0123456789ABCDEF";

			color = "#";

				for(let i = 0; i < 6; i++){
					color = color + symbols[Math.floor(Math.random() * 16)];
				}
			x++;
			
			let c = document.querySelector(".container1 .card:nth-child("+ x +") .circle").style.backgroundColor = color; 
			let e = document.querySelector(".container1 .card:nth-child("+ x +") .circle").style.zIndex = 3; 
			let d = document.querySelector(".container1 .card:nth-child("+ x +") .content a").style.backgroundColor = color;
			let a = document.querySelector(".container1 .card:nth-child("+ x +")").style.height = "420px";
		})
	})
}else{

	fetch('https://doyax0420.herokuapp.com/api/courses/isAdmin')
	.then(res => res.json())
	.then(data => {
		 let courseData;
		//if the number of courses fetched is less than 1 display no courses
		if(data.length < 1){
			courseData = "No courses available"
		}else{
			//iterate the courses collection and display each course
			courseData = data.map(course => {
				if(course.isActive === false){

						cardFooter = 
						`	
							<a id="button-css" href="./reEnableCourse.html?courseId=${course._id}" class="btn btn-warning text-white btn-block editButton"><i class="fas fa-backward"></i> Re-Enable Course</a>
					
							<a id="button-css" href="./studentsView.html?courseId=${course._id}" class="btn btn-success text-white btn-block viewButton"><i class="far fa-eye"></i> View Students</a>
						
						`			
						return( 
							`
								<div class="card">
									<div class="circle">
										<h6>${course.name}</h6>
									</div>
									<div class="content">
										<p class="text-white"><em>PRICE: </em>${course.price}</p>
										<p class="text-white">${course.description}</p>
										${cardFooter}
									</div>
								</div>
							`
							)
				}else{				

					cardFooter = 
					`
					<a id="button-css" href="./editCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton"><i class="far fa-edit"></i> Edit</a>

					<a id="button-css" href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block dangerButton"><i class="fas fa-ban"></i> Disable Course</a>
					
					<a id="button-css" href="./studentsView.html?courseId=${course._id}" class="btn btn-success text-white btn-block viewButton"><i class="far fa-eye"></i> View Students</a>
					
					`

					return (
						`
						<div class="card">
							<div class="circle">
								<h6>${course.name}</h6>
							</div>
							<div class="content">
								<p class="text-white"><em>PRICE: </em>${course.price}</p>
								<p class="text-white">${course.description}</p>
								${cardFooter}
							</div>
						</div>
						`
					)
				}
			}).join("")
		}
		let container = document.querySelector("#coursesContainer")
		container.innerHTML = courseData
		let x = 0;

		data.map(counter => {
			
			let symbols,color;
			
			symbols ="0123456789ABCDEF";

			color = "#";

				for(let i = 0; i < 6; i++){
					color = color + symbols[Math.floor(Math.random() * 16)];
				}
			x++;
			
			let c = document.querySelector(".container1 .card:nth-child("+ x +") .circle").style.backgroundColor = color; 
			let e = document.querySelector(".container1 .card:nth-child("+ x +") .circle").style.zIndex = 3; 
		})
	})

	modalButton.innerHTML = 
	`
	<div class="col-md-3">
		<a class="modalButton" href="./addCourse.html" class="btn btn-block btn-primary"><i class="far fa-plus-square"></i> Add Course</a>
	</div>

	`
}


//let d = document.querySelector(".container1 .card:nth-child(1) .circle a").style.backgroundColor = "#fff"; 
//conditional rendering
console.log()

//fetch the courses from our API



