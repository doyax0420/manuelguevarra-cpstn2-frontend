let token = localStorage.getItem("token");

//instantiate a URLSearchParams object so we can access specific parts of the query string

let params = new URLSearchParams(window.location.search)

//get returns the value of the key passed as an argument, then we store it in a variable

let courseId = params.get('courseId')

let adminUser = localStorage.getItem("isAdmin")
let cardTitle = document.querySelector("#cardTitle")
let cardText = document.querySelector("#cardText")
let profileContainer = document.querySelector("#profileContainer")
let profileContainer1;
let setData = []
let cardText1 = '';
let cardTitle1;

let x;
let y;
if(!token || token === null){
	alert("You must login first")
	window.location.replace('./login.html')
}else{
	fetch(`https://doyax0420.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		console.log(data)
		if(data.enrollees.toString() == ''){
			//alert("hello")
			cardTitle.innerHTML = `Students Enrolled <br> <h5>NO STUDENTS ENROLLED</h5>`
			cardText.innerHTML = `Course Name`
		}else{			
			data.enrollees.map(students => {
				//console.log(students.userId + "sleepy head")
				fetch(`https://doyax0420.herokuapp.com/api/users/students`)
				.then(res => res.json())
				.then(data => {
					setData = data
					//console.log(setData)
					setData.map(enrollee => {
						//console.log(enrollee._id)
						if(enrollee._id === students.userId){
							console.log(enrollee)
							cardText1 +=  `${enrollee.fullName}<br>`
						}
					})
					cardText.innerHTML = `Students Enrolled: <br>` + cardText1
				})
				cardTitle.innerHTML = `${data.name}`				
			})
		}
	})
}
