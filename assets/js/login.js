let loginForm = document.querySelector("#logInUser")
 
loginForm.addEventListener("submit", (e) => {
	e.preventDefault()
//alert("clicke submit button")
	let email = document.querySelector("#Email").value
	let password = document.querySelector("#password").value

	
	// console.log(email)
	// console.log(password)

	if(email == "" || password == ""){
		alert("Please input your email and/or password")
	}else{
		fetch('https://doyax0420.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.access){
				//store jwt in local storage
				localStorage.setItem('token', data.access)
				//send a fetch request to decode JWT and obtrain user's ID and role
				fetch('https://doyax0420.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${data.access}`
					}
				})
				.then(res => res.json())
				.then(data => {
					localStorage.setItem('id', data._id)
					localStorage.setItem('isAdmin', data.isAdmin)
					window.location.replace("./../index.html")
				})
			}else{
				//Authentication failure
				alert("Login failed")
			}
		})
	}
})