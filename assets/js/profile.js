let token = localStorage.getItem("token");

let profileContainer = document.querySelector('#profileContainer');

if(!token || token === null){
	alert("You must login first")
	window.location.replace('./login.html')
}else{
	fetch('https://doyax0420.herokuapp.com/api/users/details', {
		headers: {
			'Authorization': `Bearer ${token}`
		}    
	})
	.then(res => res.json())
	.then(data => {
		//console.log(data)
		profileContainer.innerHTML = 
		`
			<div class="col-md-12">
				<section class="jumbotron my-5">	

					<h3 class="text-center">Full Name: ${data.fullName}</h3>
					<h3 class="text-center">Email: ${data.email}</h3>
					<h3 class="text-center">Contact Number: ${data.mobileNo}</h3>
					<h3 class="mt-5">Class History</h3>
					<table class="table">
						<thead>
							<tr>
								<th> Course ID </th>
								<th> Enrolled On </th>
								<th> Status </th>
							</tr>
						</thead>
						<tbody id="coursesContainer"></tbody>
					</table>
					<a href="./viewProfile.html?userId=${data._id}"> View Profile</a> 
				</section>
			</div>
		`

		let coursesContainer = document.querySelector("#coursesContainer");

		data.enrollments.map(course => {
			fetch(`https://doyax0420.herokuapp.com/api/courses/${course.courseId}`)
			.then(res => res.json())
			.then(data => {
				//console.log(data)

				coursesContainer.innerHTML +=
				`
				<tr>
					<td>${data.name}</td>
					<td>${course.enrolledOn}</td>
					<td>${course.status}</td>
				</tr>
				`
			})
		})
	})
}