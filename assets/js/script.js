let navItems =  document.querySelector("#navSession")

let userToken = localStorage.getItem("token")

if(!userToken || userToken === null){
	//console.log(window.location.pathname)
	if(window.location.pathname == "/manuelguevarra-cpstn2-frontend/"){
		navItems.innerHTML = 
		`
			<li class="nav-item">
				<a href="./pages/login1.html" class="nav-link text-white"">Login</a>
			</li>
		`
	}
	else{
		navItems.innerHTML = 
		`
			<li class="nav-item">
				<a href="./login1.html" class="nav-link text-white">Login</a>
			</li>

		`	
	}

		
}else {	
		if(window.location.pathname == "/manuelguevarra-cpstn2-frontend/"){
				navItems.innerHTML = 
				`
					<li class="nav-item">
						<a href="./pages/logout.html" class="nav-link text-white">Logout</a>
					</li>

					<li class="nav-item">
						<a href="./pages/profile.html" class="nav-link text-white">Profile</a>
					</li>
				`
			}
			else{
				navItems.innerHTML = 
				`
					<li class="nav-item">
						<a href="./logout.html" class="nav-link text-white">Logout</a>
					</li>

					<li class="nav-item">
						<a href="./profile.html" class="nav-link text-white">Profile</a>
					</li>
				`	
			}
}
