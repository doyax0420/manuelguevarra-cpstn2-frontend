//window.location.search returns the query string part of the URL
// console.log(window.location.search)
 
//instantiate a URLSearchParams object so we can access specific parts of the query string
let token = localStorage.getItem("token")
let userId = localStorage.getItem("id")
let params = new URLSearchParams(window.location.search)

//get returns the value of the key passed as an argument, then we store it in a variable
let courseId = params.get('courseId')


let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");

fetch(`https://doyax0420.herokuapp.com/api/courses/${courseId}`)
  .then((res) => res.json())
  .then((data) => {
   // console.log(data);

    courseName.innerHTML = data.name;
    courseDesc.innerHTML = data.description;
    coursePrice.innerHTML = data.price;
    enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>`;

    console.log(data.enrollees)
    if(data.enrollees.toString() == '')
    {
      console.log("empty: " + data.enrollees)
      enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>`;
      document.querySelector("#enrollButton").addEventListener("click", () => {
        // insert the course to our courses collection
        fetch("https://doyax0420.herokuapp.com/api/users/enroll", {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`
          },
          body: JSON.stringify({
            courseId: courseId
          })
        })
          .then((res) => {
            return res.json();
          })
          .then((data) => {
            //creation of new course successful
            if (data === true) {
              //redirect to courses page
              alert("thank you for enrolling! see you!");
              window.location.replace("./courses.html");
            } else {
              //error in creating course
              alert("something went wrong");
            }
          });
      });

    }else{
      let x;
      console.log("may laman: " + data.enrollees)
      data.enrollees.map(userData => {
        console.log("userdata: " + userData.userId + " userid: " + userId)
        if(userId.toString() === userData.userId){
          x = true
          console.log("user id matched" + userData.userId + " : " + userId)
        }else{
          x = false
          console.log("user id did not match")
        }
      })

      if(x === true){
        enrollContainer.innerHTML = `<a href="./../pages/courses.html" class="btn btn-block btn-primary"> Already Enrolled </a>`;
       }else{

        enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary"> Enroll </button>`;
        document.querySelector("#enrollButton").addEventListener("click", () => {
          // insert the course to our courses collection
          fetch("https://doyax0420.herokuapp.com/api/users/enroll", {
            method: "PUT",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`
            },
            body: JSON.stringify({
              courseId: courseId
            })
          })
            .then((res) => {
              return res.json();
            })
            .then((data) => {
              //creation of new course successful
              if (data === true) {
                //redirect to courses page
                alert("thank you for enrolling! see you!");
                window.location.replace("./courses.html");
              } else {
                //error in creating course
                alert("something went wrong");
              }
            });
        });

       }
       
    }
  });
